import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private baseUrl: string = 'https://pokeapi.co/api/v2';

  constructor(private http: HttpClient) { }

  getPokemons(offset: number = 0, limit: number = 20) {
    return this.http.get(`${this.baseUrl}/pokemon/`, {
      params: {
        'offset': offset,
        'limit': limit,
      }
    });
  }

  getPokemon(name: string) {
    return this.http.get(`${this.baseUrl}/pokemon/${name}`);
  }
}
