import { Component, OnInit, Input } from '@angular/core';

import { PokemonService } from '../../services/pokemon.service';

@Component({
  selector: 'app-pokemon-view',
  templateUrl: './pokemon-view.component.html',
  styleUrls: ['./pokemon-view.component.scss']
})
export class PokemonViewComponent implements OnInit {

  @Input()
  data: any;

  pokemon: any = [];

  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.pokemonService.getPokemon(this.data.name).subscribe((data: any) => {
      this.pokemon = {
        name: data.name,
        type: data.types[0].type.name,
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        url: data.sprites.front_default,
      }
    });
  }

}
