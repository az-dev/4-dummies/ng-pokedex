import { Component, OnInit } from '@angular/core';

import { PokemonService } from '../../services/pokemon.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  pokemons: [] = [];
  page: number = 0;
  limit: number = 20;
  private offset: number = this.page * this.limit;

  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(): void {
    this.update();
    this.pokemonService.getPokemons(this.offset, this.limit).subscribe((data: any) => {
      this.pokemons = data.results;
    });
  }

  previous() {
    if (this.page - 1 >= 0) {
      this.page -= 1;
      this.getData();
    }
  }

  next() {
    this.page += 1;
    this.getData();
  }

  updateLimit() {
    this.getData();
  }

  private update() {
    if (!(this.limit >= 3 && this.limit <= 50)) {
      this.limit = 20
    }

    this.offset = this.page * this.limit;
  }

}
